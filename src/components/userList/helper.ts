import { isEmpty, some } from 'lodash'

import { IUserItem } from './userItem/types'

export default function filterUsers(listOfUsers: IUserItem[], keyword: string): IUserItem[] {
	if (isEmpty(keyword)) {
		return listOfUsers
	}

	return listOfUsers.filter((user: IUserItem) => {
		const fullName = user.name.split(' ')

		return some(fullName, name => name.toLowerCase().startsWith(keyword.toLowerCase()))
	})
}