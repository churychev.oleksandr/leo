export interface IUserItem {
	name: string
	username: string
	isVisible: boolean
	id?: number
	email?: string
	phone?: string
	website?: string
	address?: {
		[key: string]: any
	}
	company?: {
		[key: string]: any
	}
}