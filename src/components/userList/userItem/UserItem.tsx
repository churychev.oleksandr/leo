import React, { ReactElement } from 'react'
import classname from 'classnames'

import { IUserItem } from './types'

import styles from './UserItem.module.scss'

const UserItem = (props: IUserItem): ReactElement => {
	const { name, username, isVisible } = props

	const userItemClass = classname({
		[styles.userItem]: true,
		[styles.isVisible]: isVisible,
	})

	return (
		<li className={ userItemClass }>
			<span className={ styles.name }>{ name } </span>
			<span className={ styles.userName }>@ { username }</span>
		</li>
	)
}

export { UserItem }