import React, { ReactElement, useState } from 'react'
import { isEmpty, map } from 'lodash'

import filterUsers from './helper'
import { UserItem } from './userItem/UserItem'
import { IUserItem } from './userItem/types'
import { IUserList } from './types'

import styles from './UserList.module.scss'

const UserList = (props: IUserList): ReactElement | null => {
	const { userList } = props

	const [ name, setName ] = useState('')
	const [ foundUsers, setFoundUsers ] = useState(userList)

	const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
		const keyword = e.currentTarget.value

		setFoundUsers(filterUsers(userList, keyword))

		setName(keyword)
	}

	const renderContent = (): ReactElement[] => {
		const userItemArray: ReactElement[] = []

		map(foundUsers, (userItem: IUserItem, index) => {
			const { name, username } = userItem

			userItemArray.push(
				<UserItem
					name={ name }
					username={ username }
					isVisible={ index < 3 }
				/>
			)
		})

		return userItemArray
	}

	if (isEmpty(userList)) {
		return null
	}

	return (
		<div className={ styles.userListWrapper }>
			<input
				type="search"
				value={ name }
				onChange={ handleChange }
				placeholder="Search by user name..."
				className={ styles.search }
			/>

			<ol>
				{ renderContent() }
			</ol>
		</div>
	)
}

export { UserList }