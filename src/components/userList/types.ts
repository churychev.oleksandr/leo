import { IUserItem } from './userItem/types'

export interface IUserList {
	userList: IUserItem[] | []
}