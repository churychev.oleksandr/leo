import React, { ReactElement, useEffect, useState } from 'react'
import axios from 'axios'

import { IUserItem } from '../userList/userItem/types'
import { UserList } from '../userList/UserList'

import styles from './Home.module.scss'

const Home = (): ReactElement => {
	const userList: IUserItem[] = []

	const [ users, setUsers ] = useState<IUserItem[]>(userList)
	const [ loading, setLoading ] = useState<boolean>(true)
	const [ error, setError ] = useState<string>('')

	const RenderContent = (): ReactElement => {
		if (loading) {
			return (
				<p className={ styles.loader }>Loading...</p>
			)
		}

		if (error) {
			return (
				<p className={ styles.error }>{ error }</p>
			)
		}

		return (
			<UserList userList={ users } />
		)
	}

	useEffect(() => {
		axios
			.get<IUserItem[]>('https://jsonplaceholder.typicode.com/users', {
				headers: {
					'Content-Type': 'application/json'
				},
			})
			.then(response => {
				setUsers(response.data)
				setLoading(false)
			})
			.catch(ex => {
				const error =
					ex.response.status === 404
						? 'Resource Not found'
						: 'An unexpected error has occurred'
				setError(error)
				setLoading(false)
			})
	}, [])

	return (
		<div className={ styles.container }>
			<h1 className={ styles.title }>
				Users list
			</h1>

			<RenderContent />
		</div>
	)
}

export { Home }